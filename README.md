# initssh - A POSIX-adherent shellscript for quick SSH deployment on Debian/RHEL/FreeBSD (based) systems.


## Usage:

Inside this dir, just do `./ssh_deployment.sh` and tadaa!

Also it's recommended to replace my public SSH key (`quickdep.pub`) with yours :)


### Note:
As I've recently came to find out, this code may not work on all supported platforms. So YMMV. This code will most probably be refactored in the future, so hold on to your butts.

#!/usr/bin/env sh

# A script for setting up the first SSH connection on an otherwise fresh install.

# Required: openssh-server version >6.5
# Preferred: Debian or RHEL based distros (temporary)
# MacOS support may be added in the future but so far I don't see a way to do this all in the terminal: https://superuser.com/questions/104929/how-do-you-run-a-ssh-server-on-mac-os-x

## Usage:
# cd into the directory where this script is and then execute it. 
# "sh initssh/ssh_deployment.sh" WILL NOT WORK!!!
# Instead, do cd initssh/ first and then "sh ssh_deployment.sh"

# First, let's source these functions.
. "$PWD"/functions

# Determine which command is used for service / daemon management.
# I assume Linux + FreeBSD have the "service" command while *BSD have "/etc/rc.d/" scripts.
# But if not, then maybe determine which BSD it is by checking the output of "sysctl -n kern.ostype" ?
command -v service >/dev/null && sshd_restart_cmd="service sshd restart" || sshd_restart_cmd="/etc/rc.d/sshd restart"
#command -v rcctl >/dev/null && sshd_restart_cmd="rcctl restart sshd"  # OpenBSD, but "/etc/rc.d/sshd restart" also works so ¯\_(ツ)_/¯

printf "Please specify the ssh user: "
read sshuser
if [ "$(id -u $sshuser)" -eq 0 ]
	then printf "Login as root over SSH = bad practice. Are you sure? Y/N: "
		while true ; do 
			read reply_root_confirmation
			case "$reply_root_confirmation" in
				Y|y ) sshuser=root && f__sshdep_multifun && 
						# The following line uses ex (vi) to replace PermitRootLogin string in /etc/ssh/sshd_config to allow root login. Thanks Soliton!
						ex -sc 'g/PermitRootLogin no/s//PermitRootLogin yes/|wq' /etc/ssh/sshd_config &&
						$sshd_restart_cmd
						echo ; echo "Try logging in now over SSH as root" && exit 0
						;;
					*	) echo "Wise choice :)" ; exit 0 ;;
			esac
		done
	elif id -u "$sshuser"
		then # Check if sudo or doas exist, are executable by current user, and if yes, proceed. If neither are found / aren't executable, then abort.
			if sudo echo >/dev/null
				then privprefix=sudo && f__sshdep_multifun ; $privprefix $sshd_restart_cmd ; exit 0
				else echo "sudo not found / not configured properly!"
			fi
			if doas echo >/dev/null
				then privprefix=doas && f__sshdep_multifun ; $privprefix $sshd_restart_cmd
				elif [ "$(id -u)" -eq 0 ]
					then
						echo "doas not found / is not configured properly! Script will still continue since it is run with root privs :)"
						f__sshdep_multifun ; $sshd_restart_cmd ; exit 0
				else
					echo "doas not found / is not configured properly!" && echo "Try to run this script as root maybe?" && exit 1
			fi
			echo ; echo "Try logging in now over SSH as $sshuser" && exit 0
		else echo "User does not exist. Please try again." && exit 1
fi

echo

# Restart sshd
$privprefix $sshd_restart_cmd
